module GooglePage
  @google_search_text_box = "input[type='text']"
  @google_search_button = "//div[@class='FPdoLc VlcLAe']//input[1]"
  def self.search_for(search_word)
    page.set_text(@google_search_text_box, search_word)
    page.click(@google_search_button)
  end
end