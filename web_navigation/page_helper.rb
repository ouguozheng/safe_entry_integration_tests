module PageHelper
  def page
    Capybara.current_session
  end

  def resize_browser(resolution_length, resolution_width)
    page.current_window.resize_to(resolution_length, resolution_width)
  end

  def set_text(element, value, timeout=nil, disabled=false)
    node = find_element(element, timeout, disabled)
    execute_action("Unable to set text in element: #{element}\n") do
      node.set(value)
    end
  end

  def save_screenshot(path)
    page.save_screenshot(path)
  end

  def select_option(element, value, timeout=nil, disabled=false)
    node = find_element(element, timeout, disabled)
    execute_action("Unable to select option #{value} in dropdown list element: #{element}\n") do
      node.select(value)
    end
  end

  def find_elements(element, timeout=nil, disabled=false)
    options = {}
    options[:wait] = timeout unless timeout.nil?
    options[:visible] = disabled
    execute_action("Unable to find elements: #{element}\n") do
      page.all(locator(element), element, options)
    end
  end

  def find_element(element, timeout=nil, disabled=false)
    options = {}
    options[:wait] = timeout unless timeout.nil?
    options[:visible] = disabled
    execute_action("Unable to find specified element: #{element}\n") do
      page.find(locator(element), element, options)
    end
  end

  def has_element?(element, timeout=nil, disabled=false)
    options = {}
    options[:wait] = timeout unless timeout.nil?
    options[:visible] = disabled
    page.send("has_#{locator(element).to_s}?", locator(element), options)
  end

  def click(element, timeout=nil, disabled=false)
    node = find_element(element, timeout, disabled)
    execute_action("Unable to click on element: #{element}\n") do
      node.click
    end
  end

  def right_click(element, timeout=nil, disabled=false)
    node = find_element(element, timeout, disabled)
    execute_action("Unable to click on element: #{element}\n") do
      node.right_click
    end
  end

  def checked?(element, timeout=nil, disabled=false)
    node = find_element(element, timeout, disabled)
    execute_action("Unable to check if checkbox #{element} is checked\n") do
      node.checked?
    end
  end

  def check(element, timeout=nil, disabled=false)
    # Advisable that checkbox element is a label[for='value']
    node = find_element(element, timeout, disabled)
    return if node.checked?
    execute_action("Unable to check checkbox #{element}\n") do
      node.click
    end
  end

  def uncheck(element, timeout=nil, disabled=false)
    # Advisable that checkbox element is a label[for='value']
    node = find_element(element, timeout, disabled)
    return unless node.checked?
    execute_action("Unable to uncheck checkbox #{element}\n") do
      node.click
    end
  end

  def within_iframe(iframe_element, timeout=nil, disabled=false)
    raise "No block given when using within_iframe method!!!" unless block_given?
    node = find_element(iframe_element, timeout, disabled)
    execute_action("Unable to execute action within iframe #{iframe_element}\n") do
      within_frame(node) do
        yield if block_given?
      end
    end
  end

  def refresh_page
    page.refresh
  end

  def open_new_tab
    execute_action("Unable to open a new tab!!!") do
      Capybara.current_session.driver.open_new_window
    end
  end

  def get_all_window_handles
    Capybara.current_session.windows
  end

  def switch_to_another_window (window_handle)
    execute_action("Unable to swtich to tab: #{window_handle}") do
      Capybara.current_session.switch_to_window(window_handle)
    end
  end

  private
  def execute_action(error_message)
    begin
      yield
    rescue => e
      log_message(:error, 1, error_message) do
        raise_error(e, error_message)
      end
    end
  end

  def raise_error(e, error_message, addtional_message=nil)
    screenshot_path = TestManager.screenshots_directory+"/#{TestManager.current_scenario.name.downcase.gsub(' ','_')}.png"
    save_screenshot(screenshot_path)
    error_message += addtional_message+"\n" unless addtional_message.nil?
    error_message += e.message
    raise error_message
  end

  def locator(element)
    (element.match(/^\/\//).nil? && element.match(/^\//).nil?) ? :css : :xpath
  end
end