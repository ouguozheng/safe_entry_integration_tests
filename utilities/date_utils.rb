module DateUtils
  def self.now
    Date.current
  end

  def self.yesterday
    Date.yesterday
  end

  def self.tomorrow
    Date.tomorrow
  end

  def self.days_before_today(days)
    Date.current.prev_day days
  end

  def self.days_after_today(days)
    Date.current.next_day days
  end

  def self.days_before_date(date, days)
    date - days
  end

  def self.days_after_date(date, days)
    date + days
  end

  def self.months_before_today(months)
    Date.current.prev_month months
  end

  def self.months_after_today(months)
    Date.current.next_month months
  end

  def self.months_before_date(date, months)
    date - months.month
  end

  def self.months_after_date(date, months)
    date + months.month
  end

  def self.years_before_today(years)
    Date.current.prev_year years
  end

  def self.years_after_today(years)
    Date.current.next_year years
  end

  def self.years_before_date(date, years)
    date + years.year
  end

  def self.years_before_date(date, years)
    date - years.year
  end
end
