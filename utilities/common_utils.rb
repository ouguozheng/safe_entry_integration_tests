module CommonUtils
  def wait_for_condition(retry_count = 10)
    counter = 1
    result = nil
    while counter <= retry_count
      begin
        result = yield
        break unless result.nil?
      rescue => e
        puts 'Error occurred during operation:' + e.message
      end
      counter += 1
    end
    result
  end
end