Given(/^I like (vanilla|chocolate|yuzu|macha) ice cream$/) do |flavour_of_ice_cream|
  puts "I like #{flavour_of_ice_cream}"
  log_message(:info, "hihi")
end

And(/^I use google$/) do
  page.visit('https://www.google.com.sg/')
end

And(/^binding pry$/) do
  binding.pry
end

And(/^I raised an error$/) do
  raise "RAWWRRR!!!"
end

And(/^I search for (.*)$/) do |search_word|
  GooglePage.search_for(search_word)
end

Given(/^I particularly love (vanilla) ice cream$/) do |favourite_flavour|
  puts "LOVES #{favourite_flavour} <3"
end