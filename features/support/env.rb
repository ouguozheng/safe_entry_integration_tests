require 'pry'
require 'capybara'
require 'selenium-webdriver'
require 'active_record'
require 'active_support/all'
require 'faker'


# include Capybara::Angular::DSL

# env.rb file will be ran first everytime you start the automation suite
$PROJECT_ROOT = File.expand_path(File.join(File.dirname(__FILE__), '..', '..'))
require File.join($PROJECT_ROOT, 'config', 'test_manager.rb')
require File.join($PROJECT_ROOT, 'driver_management', 'driver_setup.rb')
require File.join($PROJECT_ROOT, 'test_exceptions', 'exceptions.rb')

TestManager.init($PROJECT_ROOT)

require File.join($PROJECT_ROOT, 'config', 'database_connection.rb')

['config', 'pages', 'models', 'api', 'data_helpers', 'driver_management', 'web', 'utilities', 'web_navigation'].each do |project_subdirectory|
  Dir.glob(File.join(TestManager.project_root_path, project_subdirectory, '**', '*.rb')).each do |file|
    require file
  end
end

include PageHelper
include AutomationLogger
include DataHelper
include CommonUtils
include DateUtils
include ApiHelper

Capybara.run_server = false
Capybara.app_host = 'https://www.google.com.sg/'
DriverSetup.setup_drivers
DriverSetup.select_driver(TestManager.driver)
