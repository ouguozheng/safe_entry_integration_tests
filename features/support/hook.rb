def initializing_all_automation_variables
  $used_nrics = []
  $used_fins = []
end

Signal.trap("INT") do |signal|
  puts "Kill signal #{Signal.signame(signal)} sent... exiting automation abruptly..."
  exit
end

Before do |scenario|
#   This hook is used for operations you want only ran once in the lifetime of the running of the automation suite
  $run_once_only = false
  return if $run_once_only
  initializing_all_automation_variables
  TestManager.clear_screenshots_folder
  $run_once_only = true
end

Before do |scenario|
  @scenario_start_time = Time.now
  @scenario_step_time = Time.now
  TestManager.current_scenario = scenario
  puts "\n================Scenario Start: #{scenario.name}================\n"
end

After do |scenario|
  puts "\n================Scenario Ends: #{scenario.name}================\n"
  scenario_time = Time.now - @scenario_start_time

  puts "\n================Scenario ended: #{scenario_time} seconds================\n"
  Capybara.reset_sessions!
  if scenario.failed?
    TestManager.add_to_list_of_failing_scenarios(scenario.name)
  end
  TestManager.increase_test_count
end

AfterStep do |_|
  scenario_step_time = Time.now - @scenario_step_time
  puts "\nStep took: #{scenario_step_time} seconds\n"
  @scenario_step_time = Time.now
end

at_exit do
  TestManager.calculate_test_statistics
end

