Feature: Test Feature

  Background:
    Given I particularly love vanilla ice cream

  Scenario: Test Scenario
    Given I like vanilla ice cream
    And I use google
    And I search for vanilla ice cream

  Scenario Outline: Test Scenario Outline
    Given I like <flavours_of_ice_cream> ice cream
    Examples:
    | flavours_of_ice_cream |
    | vanilla               |
    | chocolate             |
    | yuzu                  |
    | macha                 |



