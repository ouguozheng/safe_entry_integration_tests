class DatabaseConnection < ActiveRecord::Base
  # All models which correspond to a table in database, must inherit this base class
  self.abstract_class = true
  begin
    ActiveRecord::Base.establish_connection({
        host: TestManager.database_hostname,
        username: TestManager.database_username,
        password: TestManager.database_password,
        adapter: 'oracle_enhanced',
        port: TestManager.database_port,
        pool: 5,
        timeout: 5000,
        database: TestManager.database_name }
    )
  rescue => e
    error_message = "Unable to establish connection with the database!!\n"
    error_message += e.message
    raise error_message
  end
end