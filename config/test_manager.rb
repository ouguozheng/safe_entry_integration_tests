module TestManager
  SUPPORTED_ENV_VARS = {
      platform: 'platform',
      browser: 'browser',
      env: 'env'
  }

  SUPPORTED_TEST_ENV = ['local', 'dev-automation', 'release-automation']
  SUPPORTED_PLATFORMS = ['windows']

  DEFAULT_PLATFORM = 'windows'
  DEFAULT_BROWSER = 'chrome'
  DEFAULT_ENV = 'local'
  LOCALTIMEOUT = 5
  ENV_TIMEOUT = 60
  WAIT_TIME_TO_LOG_IN_SECONDS = 60

  LOG_NAME = 'automation_log.log'
  def self.init(project_root)
    set_default_date_time_format
    @project_root = project_root
    @list_of_failing_scenarios = []
    @number_of_test_scenarios_rans = 0
    @browser = set_browser
    @platform = set_platform
    @screenshots_directory = set_screenshots_folder
    @test_results = set_test_results_folder
    @env = set_test_environment
    @timeout = set_timeout
    @write_to_log_file_enabled = set_write_to_log_file_enabled
    process_test_environment_configurations
    set_maximum_wait_timeout_for_capybara(10)
    initialize_global_variables

    @current_screnario = nil
    @logs_directory = set_logs_folder if write_to_log_file_enabled?
    @log_file = set_logs_file if write_to_log_file_enabled?
    @previous_log_time = initialize_logging_time
  end

  def self.current_scenario
    @current_screnario
  end

  def self.current_scenario=(scenario)
    @current_screnario = scenario
  end

  def self.log_time
    @previous_log_time
  end

  def self.update_log_timer(time=Time.zone.now)
    @previous_log_time = time
  end

  def self.logs_directory
    @logs_directory
  end

  def self.log_file
    @log_file
  end

  def self.write_to_log_file_enabled?
    @write_to_log_file_enabled
  end

  def self.project_root_path
    @project_root
  end

  def self.browser
    @browser
  end

  def self.platform
    @platform
  end

  def self.screenshots_directory
    @screenshots_directory
  end

  def self.test_results_directory
    @test_results
  end

  def self.env
    @env
  end

  def self.timeout
    @timeout
  end

  def self.driver
    return DriverSetup::DRIVERS[:chrome] if @browser.nil?
    return DriverSetup::DRIVERS[@browser.to_sym] unless DriverSetup::DRIVERS[@browser.to_sym].nil?
    raise "Unsupported driver: #{@browser} specified"
  end

  def self.add_to_list_of_failing_scenarios(scenario_name)
    @list_of_failing_scenarios << scenario_name
  end

  def self.any_failing_scenarios?
    @list_of_failing_scenarios.empty?
  end

  def self.increase_test_count
    @number_of_test_scenarios_rans+=1
  end

  def self.calculate_test_statistics
    puts "Number of tests ran: #{@number_of_test_scenarios_rans}"
    puts "Number of tests passed: #{@number_of_test_scenarios_rans - @list_of_failing_scenarios.length}"
    puts "Number of tests failed: #{@list_of_failing_scenarios.length}"
  end

  def self.clear_screenshots_folder
    if File.directory?(@screenshots_directory)
      Dir.glob(@screenshots_directory+'/*.png').each do |file_path|
        begin
          File.delete(file_path)
        rescue
          raise "Unable to delete file at: #{file_path}"
        end
      end
    end
  end

  private
  def self.set_default_date_time_format
    Date::DATE_FORMATS[:default] = "%e %b %Y"
    Time::DATE_FORMATS[:default] = "%H:%M"
    Time.zone = 'Singapore'
  end

  def self.initialize_logging_time
    Time.now
  end

  def self.set_logs_folder
    screenshots_directory = @project_root+'/logs'
    unless File.directory?(@project_root+'/logs')
      Dir.mkdir(screenshots_directory)
    end

    screenshots_directory
  end

  def self.set_logs_file
    if @logs_directory.present?
      if File.exists?(@logs_directory+"/#{LOG_NAME}")
        begin
          FileUtils.remove @logs_directory+"/#{LOG_NAME}"
        rescue => e
          raise "Error removing file #{@logs_directory}/#{LOG_NAME}\n#{e}"
        end
      end
      file = File.new(@logs_directory+"/#{LOG_NAME}", "w")
      file.close
    end

    @logs_directory+"/#{LOG_NAME}"
  end

  def self.set_write_to_log_file_enabled
    @write_to_log_file_enabled = ENV['set_write_to_log_file_enabled'].present? && ENV['set_write_to_log_file_enabled'] == 'true' ? true : false
  end

  def self.initialize_global_variables
    $valid_nrics = Set.new
    $valid_fins = Set.new
  end

  def self.set_timeout
    @env == DEFAULT_ENV ? LOCALTIMEOUT : ENV_TIMEOUT
  end

  def self.set_test_environment
    return DEFAULT_ENV if ENV[SUPPORTED_ENV_VARS[:env]].nil? or SUPPORTED_TEST_ENV.exclude?(ENV[SUPPORTED_ENV_VARS[:env]])
    ENV[SUPPORTED_ENV_VARS[:env]]
  end

  def self.set_screenshots_folder
    screenshots_directory = @project_root+'/screenshots'
    if File.directory?(screenshots_directory)
      files = [File.join(screenshots_directory, '*.png'),
               File.join(screenshots_directory, '*.jpg')]
      FileUtils.rm_f(Dir.glob(files))
    else
      File.mkdir(screenshots_directory)
    end

    screenshots_directory
  end

  def self.set_test_results_folder
    test_results = @project_root+'/test_results'
    if File.directory?(test_results)
      files = [File.join(test_results, '*.json'),
               File.join(test_results, '*.html')]
      FileUtils.rm_f(Dir.glob(files))
    else
      Dir.mkdir(test_results)
    end
  end

  def self.set_browser
    return DEFAULT_BROWSER if ENV[SUPPORTED_ENV_VARS[:browser]].nil? or DriverSetup::DRIVERS.keys.exclude?(ENV[SUPPORTED_ENV_VARS[:browser]].to_sym)
    ENV[SUPPORTED_ENV_VARS[:browser]]
  end

  def self.set_platform
    return DEFAULT_PLATFORM if ENV[SUPPORTED_ENV_VARS[:platform]].nil? or SUPPORTED_PLATFORMS.exclude?(ENV[SUPPORTED_ENV_VARS[:platform]])
    ENV[SUPPORTED_ENV_VARS[:platform]]
  end

  def self.set_maximum_wait_timeout_for_capybara(timeout=nil)
    Capybara.default_max_wait_time = timeout unless timeout.nil?
  end

  def self.process_test_environment_configurations
    environment_configuration = YAML.load(read_file("#{@project_root}/config/environment.yaml"))[@env]
    raise "Something went wrong went reading the file environment.yaml file! No environment configurations were ported over!!!" if environment_configuration.blank?
    environment_configuration.each do |env_config_key, env_config_value|
      self.class.send(:define_method, env_config_key.to_sym) { env_config_value }
    end
  end

  def self.read_file(file_path)
    begin
      File.read(file_path)
    rescue
      raise "Unable to read file: #{file_path}"
    end
  end
end