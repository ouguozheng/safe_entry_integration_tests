module AutomationLogger
  LOGTYPE = {
      error: 'error',
      info: 'info',
      warn: 'warn'
  }

  def log_message(log_type, message, options={})
    #TODO
    final_log_message =  "#{log_type(log_type).to_s} - #{message}"
    puts final_log_message
    write_to_log(final_log_message) if TestManager.write_to_log_file_enabled?
    yield if block_given?
  end

  private
  TIME_DURATION_BEFORE_LOG_TO_FILE_IN_SECONDS = 5

  def write_to_log(log_message)
    # only write to log file every 5 seconds
    if can_write_to_log?
      add_to_log_buffer(log_message)
      write_to_log_file
      TestManager.update_log_timer
      @buffer = nil
    else
      add_to_log_buffer(log_message)
    end
  end

  def add_to_log_buffer(log_message)
    if @buffer.nil?
      @buffer = log_message+"\n"
    else
      @buffer += log_message+"\n"
    end
  end

  def write_to_log_file
    unless @buffer.nil?
      begin
        File.open(TestManager.log_file, 'a+') { |file|
          file.write(@buffer)
        }
      rescue => e
        raise "Unable to write to log file!!!\n#{e}"
      end
    end
  end

  def can_write_to_log?
    ( Time.now - TestManager.log_time ) > TIME_DURATION_BEFORE_LOG_TO_FILE_IN_SECONDS
  end

  def log_type(log_type)
    case log_type
    when :info
      LOGTYPE[:info]
    when :error
      LOGTYPE[:error]
    when :info
      LOGTYPE[:warn]
    else
      raise "Invalid log level is specified!!!"
    end
  end
end