module DataHelper
  SG_MOBILE_PHONE_NUMBER = '9'
  SG_HOME_PHONE_NUMBER = ['6','8']
  ALPHABETS = ('A'..'Z').to_a
  NUMBERS = (0..9).to_a
  SG_POSTAL_DISTRICT = ['01', '02', '03', '04', '05', '07', '08', '14', '15', '16', '09', '10', '11', '12', '13', '17', '18', '19']
  GENDER = ['male', 'female']

  def valid_postal_code
    SG_POSTAL_DISTRICT.sample + random_number(4).to_s
  end

  def valid_unit_no
    random_number(4) + 'A'
  end

  def valid_building_name
    #TODO
  end

  def valid_floor_no
    random_number(2)
  end

  def valid_block_no
    random_alphanumeric(3)
  end

  def valid_street_name
    Faker::Address.street_name
  end

  def random_gender
    GENDER.sample
  end

  def random_alphabets
    ALPHABETS.sample
  end

  def random_alphabets(size=1)
    ALPHABETS.sample(size).join
  end

  def random_alphanumeric(size)
    (ALPHABETS + NUMBERS ).sample(size).join
  end

  def valid_mobile_phone
    SG_MOBILE_PHONE_NUMBER+ ::Faker::Number.number(7)
  end

  def valid_house_number
    SG_HOME_PHONE_NUMBER.sample+ ::Faker::Number.number(7)
  end

  def valid_cpf_no
    random_alphabets + random_number(5) + random_alphabets
  end

  def valid_fin
    generated_value = wait_for_condition do
      fin = new_fin

      if $valid_fins.include?(fin)
        nil
      else
        $valid_fins.add(fin)
        fin
      end
    end
    raise 'Unable to generate unique FIN !!!' if generated_value.nil?
    generated_value
  end

  def valid_nric
    generated_value = wait_for_condition do
      nric = new_nric
      if $valid_nrics.include?(nric)
        nil
      else
        $valid_nrics.add(nric)
        nric
      end
    end
    raise 'Unable to generate unique NRIC !!!' if generated_value.nil?
    generated_value
  end

  def random_number(num_of_numbers=1)
    Faker::Number.number(num_of_numbers)
  end

  private
  def new_fin
    fin_digits = random_number(7)
    first_char = %w(F G).sample
    map = {
        0 => 'K',
        1 => 'L',
        2 => 'M',
        3 => 'N',
        4 => 'P',
        5 => 'Q',
        6 => 'R',
        7 => 'T',
        8 => 'U',
        9 => 'W',
        10 => 'X'
    }
    first_char + fin_digits + valid_checksum(first_char, fin_digits, map)
  end

  def new_nric
    fin_digits = random_number(7)
    first_char = %w(S T).sample
    map = {
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        8 => 'I',
        9 => 'Z',
        10 => 'J'
    }
    first_char + fin_digits + valid_checksum(first_char, fin_digits, map)
  end

  def valid_checksum(first_char, fin_digits, map)
    checksum = (2 * fin_digits[0].to_i +
        7 * fin_digits[1].to_i +
        6 * fin_digits[2].to_i +
        5 * fin_digits[3].to_i +
        4 * fin_digits[4].to_i +
        3 * fin_digits[5].to_i +
        2 * fin_digits[6].to_i)
    checksum += 4 if %w(T G).include?(first_char)
    remainder = 10 - (checksum % 11)
    map[remainder]
  end
end