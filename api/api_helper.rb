module ApiHelper
  HTTPS = 'https'
  def http_sample_get_call(url)
    uri = URI.parse(url)
    http_connect(:get, uri)
  end

  def http_sample_post_call(url, http_payload=nil)
    uri = URI.parse(url)
    http_connect(:post, uri, http_payload)
  end

  private
  def http_connect(http_methods, uri, http_payload=nil)
    raise "Please use a URI object!!" unless uri.is_a?(URI)
    begin
      Net::HTTP.start(uri.host, uri.port, use_ssl: use_ssl?(uri)) do |http|
        request = http_method_type(http_methods, uri)
        request.body = http_payload unless http_payload.nil?
        decode_response(http.request(request))
      end
    rescue => e
      raise e
    end
  end

  def decode_response(response)
    if response.is_a?(Net::HTTPSuccess) && response.is_a?(Net::HTTPOK)
      return response.body
    elsif response.is_a?(Net::HTTPSuccess)
      log_message(:warn, "Successful response but response code is: #{response.code}")
      return response.body
    elsif response.is_a?(Net::HTTPInternalServerError) && response.is_a?(Net::HTTPServerError)
      log_message(:error, "Unable to connect!!! Reponse code: #{response.code}")
      return response.body
    elsif response.is_a?(Net::HTTPClientError)
      log_message(:error, "Data error!!! Reponse code: #{response.code}")
      return response.body
    else
      log_message(:error, "Error in the response!!! Response code: #{response.code}")
      return response.body
    end
  end

  def use_ssl?(uri)
    uri.scheme == HTTPS
  end

  def http_method_type(call_type, uri)
    case call_type
    when :get
      Net::HTTP::Get.new uri
    when :post
      Net::HTTP::Post.new uri
    when :put
      Net::HTTP::Put.new uri
    when :delete
      Net::HTTP::Delete.new uri
    else
      Net::HTTP::Get.new uri
    end
  end
end