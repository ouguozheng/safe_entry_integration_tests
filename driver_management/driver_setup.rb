module DriverSetup
  DRIVERS = {
      chrome: 'chrome',
      firefox: 'firefox',
      ie: 'ie',
      edge: 'edge',
      safari: 'safari',
      headless: 'headless',
      chrome_remote: 'chrome_remote',
      safari_remote: 'safari_remote',
      edge_remote: 'edge_remote',
      firefox_remote: 'firefox_remote',
      ie_remote: 'ie_remote'
  }

  SELENIUM_GRID_SERVER = "http://127.0.0.1:4444/wd/hub"
#   Where all driver setups are done. Please put all selenium driver configurations here.
  def self.firefox_options
    options = {}
    options[:profile] = firefox_profile
    options = {:marionette => true}
    http_client = Selenium::WebDriver::Remote::Http::Default.new
    http_client.read_timeout = 200
    http_client.open_timeout = 200
    options[:http_client] = http_client
    options[:desired_capabilities] = Selenium::WebDriver::Remote::Capabilities.firefox()
    options[:service] = Selenium::WebDriver::Service.firefox({ args: { driver_path: 'webdrivers/firefox/geckodriver' } })
    options
  end

  def self.chrome_options
    options = {:browser => :chrome}
    http_client = Selenium::WebDriver::Remote::Http::Default.new
    http_client.read_timeout = 200
    http_client.open_timeout = 200
    options[:http_client] = http_client
    chrome_options = Selenium::WebDriver::Chrome::Options.new
    # chrome_options.add_argument("--proxy-pac-url=file://#{$PROJECT_ROOT}/config/braintree_proxy_for_chrome.js") unless TestManager.dev_environment?
    # chrome_options.add_argument("--incognito=true")
    options[:desired_capabilities] = Selenium::WebDriver::Remote::Capabilities.chrome()
    options[:options] =  chrome_options
    options[:service] = Selenium::WebDriver::Service.chrome({ args: { driver_path: 'webdrivers/chrome/chromedriver' } })
    options
  end

  def self.ie_options
    {:browser => :remote, :url => "http://#{ENV['HUB']}:4444/wd/hub", :desired_capabilities => Selenium::WebDriver::Remote::Capabilities.internet_explorer}
  end

  def self.edge_options
    options = {:browser => :remote}
    options[:desired_capabilities] = Selenium::WebDriver::Remote::Capabilities.edge(platform:"#{TestManager.platform}")
    options[:url] = SELENIUM_GRID_SERVER
    options
  end

  def self.chrome_mobile_options
    options = {:browser => :chrome}
    http_client = Selenium::WebDriver::Remote::Http::Default.new
    http_client.read_timeout = 200
    http_client.open_timeout = 200
    options[:http_client] = http_client

    # ChromeDriver restrictions: You can only either use device_name OR device_metrics attribute when you add emulation. You cannot set both attributes at the same time.
    chromeOptions = Selenium::WebDriver::Chrome::Options.new
    chromeOptions.add_emulation(device_metrics: { "width" => 480, "height" => 862, "pixelRatio" => 1.0, touch: false } )
    options[:options] = chromeOptions
    caps = Selenium::WebDriver::Remote::Capabilities.chrome
    options[:desired_capabilities] = caps
    options
  end

   def self.remote_options
     options = {:browser => :remote}
     options[:desired_capabilities] = remote_desired_capabilities
     options[:url] = SELENIUM_GRID_SERVER
     options
   end

  def self.safari_options
    options = {:browser => :remote}
    options[:desired_capabilities] = Selenium::WebDriver::Remote::Capabilities.safari(platform:"#{TestManager.platform}")
    options[:url] = SELENIUM_GRID_SERVER
    options
  end

  def self.remote_desired_capabilities
    caps = case TestManager.browser
           when DRIVERS[:chrome]
             Selenium::WebDriver::Remote::Capabilities.chrome({platform:TestManager.platform})
           when DRIVERS[:ie]
             Selenium::WebDriver::Remote::Capabilities.internet_explorer({platform:'windows'})
           when DRIVERS[:firefox]
             Selenium::WebDriver::Remote::Capabilities.firefox(platform:"#{TestManager.platform}")
           when DRIVERS[:edge]
             Selenium::WebDriver::Remote::Capabilities.edge(platform:"#{TestManager.platform}")
           when DRIVERS[:safari]
             Selenium::WebDriver::Remote::Capabilities.safari(platform:"#{TestManager.platform}")
           else
             raise "This automation suite currently do not support this browser #{TestManager.browser}"
           end
    caps
  end

  def self.setup_drivers
    # Setup all necessary drivers
    # firefox browser. Requires geckodriver
    Capybara.register_driver :firefox do |app|
      Capybara::Selenium::Driver.new(app, firefox_options)
    end

    # chrome browser. Requires chromedriver
    Capybara.register_driver :chrome do |app|
      Capybara::Selenium::Driver.new(app, chrome_options)
    end

    #
    Capybara.register_driver :ie do |app|
      Capybara::Selenium::Driver.new(app, ie_options)
    end

    # Edge browser. Requires edge driver
    Capybara.register_driver :edge do |app|
      Capybara::Selenium::Driver.new(app, edge_options)
    end

    Capybara.register_driver :safari do |app|
      Capybara::Selenium::Driver.new(app, safari_options)
    end

    # chrome mobile browser.
    Capybara.register_driver :chrome_mobile do |app|
      Capybara::Selenium::Driver.new(app, chrome_mobile_options)
    end

    # remote
    Capybara.register_driver :remote do |app|
      Capybara::Selenium::Driver.new(app, remote_options).tap do |driver|
        driver.browser.file_detector = lambda do |args|
          str = args.first.to_s
          str if File.exist?(str)
        end
      end
    end

    Capybara.register_driver :chrome_remote do |app|
      Capybara::Selenium::Driver.new(app, remote_options).tap do |driver|
        driver.browser.file_detector = lambda do |args|
          str = args.first.to_s
          str if File.exist?(str)
        end
      end
    end

    Capybara.register_driver :edge_remote do |app|
      Capybara::Selenium::Driver.new(app, remote_options).tap do |driver|
        driver.browser.file_detector = lambda do |args|
          str = args.first.to_s
          str if File.exist?(str)
        end
      end
    end

    Capybara.register_driver :firefox_remote do |app|
      Capybara::Selenium::Driver.new(app, remote_options).tap do |driver|
        driver.browser.file_detector = lambda do |args|
          str = args.first.to_s
          str if File.exist?(str)
        end
      end
    end

    Capybara.register_driver :ie_remote do |app|
      Capybara::Selenium::Driver.new(app, remote_options).tap do |driver|
        driver.browser.file_detector = lambda do |args|
          str = args.first.to_s
          str if File.exist?(str)
        end
      end
    end

    Capybara.register_driver :safari_remote do |app|
      Capybara::Selenium::Driver.new(app, remote_options).tap do |driver|
        driver.browser.file_detector = lambda do |args|
          str = args.first.to_s
          str if File.exist?(str)
        end
      end
    end
  end

  def self.select_driver(driver_choice)
    case driver_choice
    when DRIVERS[:chrome]
      Capybara.current_driver = :chrome
      Capybara.default_driver = :chrome
    when DRIVERS[:firefox]
      Capybara.current_driver = :firefox
      Capybara.default_driver = :firefox
    when DRIVERS[:ie]
      Capybara.current_driver = :ie
      Capybara.default_driver = :ie
      Capybara.javascript_driver = :ie
    when DRIVERS[:edge]
      Capybara.current_driver = :edge
      Capybara.default_driver = :edge
    when DRIVERS[:safari]
      Capybara.current_driver = :safari
      Capybara.default_driver = :safari
    when DRIVERS[:remote]
      Capybara.current_driver = :remote
      Capybara.default_driver = :remote
    when DRIVERS[:headless]
      Capybara.current_driver = :selenium_chrome_headless
      Capybara.default_driver = :selenium_chrome_headless
    when DRIVERS[:chrome_remote]
      Capybara.current_driver = :chrome_remote
      Capybara.default_driver = :chrome_remote
    when DRIVERS[:safari_remote]
      Capybara.current_driver = :safari_remote
      Capybara.default_driver = :safari_remote
    when DRIVERS[:edge_remote]
      Capybara.current_driver = :edge_remote
      Capybara.default_driver = :edge_remote
    when DRIVERS[:firefox_remote]
      Capybara.current_driver = :firefox_remote
      Capybara.default_driver = :firefox_remote
    when DRIVERS[:ie_remote]
      Capybara.current_driver = :ie_remote
      Capybara.default_driver = :ie_remote
    when DRIVERS[:chrome_mobile]
      Capybara.current_driver = :chrome_mobile
      Capybara.default_driver = :chrome_mobile
    else
      Capybara.current_driver = :webkit
      Capybara.default_driver = :webkit
      Capybara.javascript_driver = :webkit
    end
  end

  private

  def self.firefox_profile
    Selenium::WebDriver::Firefox::Profile.new
  end
end